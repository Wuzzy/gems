# textdomain: gems_battle
# author(s):
# reviewer(s):

##[ init.lua ]##
Ruby=
Emerald=
Sapphire=
Opal=
Gems Battle=

##[ api.lua ]##
@1 team has been eliminated!=
You Died!=
Rubies=
Emeralds=
Opals=
Sapphires=

##[ armor.lua ]##
## @1 is a gem name
@1 Shield=

##[ editor.lua ]##
Gems Battle Arena Settings=
Edit Great Gem Locations=
Place Mines=
Edit Shop Locations=
@1 Great Gem=
@1 Shop=
Jail Spawn=
Jail has been set to: @1=

##[ entities.lua ]##
@1 Gem=
Great Gem under attack!=


##[ hud.lua ]##
In Jail: @1=

##[ items.lua ]##
@1 Revealer=
@\nUse to reveal the current location@\nof all @1 team members to your team.@\nDisappears after 5 sec and does not update.=
@1 Team isn't in this game!=
@1 Pickaxe=
@\nUse to break Gem blocks and Great Gems, especially @1 Great Gems.=
@1 Sword=
@\nMost useful against the @1 team=

##[ minigame_manager.lua ]##
Get Gems From Gem Mines, @\nBuy Gem Tools at the shops!=
Destroy the other teams' Great Gem @\nwith a Gem Pick so you can eliminate them!=
Fight!=
@1 team's Great Gem has been destroyed! They will not respawn!=
@1 team's Great Gem has been destroyed!=
Time's up! No one won the game.=

##[ nodes.lua ]##
@1 Block=
@1 Glass=
@1 Mine=
@1 Protector=
You can't place protectors within 4 nodes of another protector!=

##[ shop.lua ]##
A strong block that can only @\nbe broken with gem picks=
A block that is strong @\nagainst normal tools=
A pick to break Great Gems. Works FASTEST against @1 Great Gems.=
A Powerful Sword. Kills @1 players faster.=
Reveals the current location @\nof players on @1 team to all your team members for a short time.@\n Recharges slowly.=
Damages all players in a radius EXCEPT @1 team players.@\n Cannot be placed near other protectors.=
A normal steel Pick. CANNOT break Great Gems!=
A normal steel Sword=
A normal steel Axe=
Stone - A good building material=
Obsidian - A tough block=
Wood - A good building material=
Tree - A tough block=
Glass - A good building material=
Obsidian Glass - Can only be broken with a pick=
Heals one heart. Cooldowns apply=
Enderpearl - Teleports you when thrown=
Torch - Light up those dark spaces=
Helmet - A good protection against damage=
Chestplate - A good protection against damage=
Leggings - A good protection against damage=
Boots - A good protection against damage=
[!] You don't have enough @1!=
## Example: Hi! Bring me Rubies!
Hi! Bring me @1!=
I will sell you my wares!
Just take whatever you like!=






