local S = minetest.get_translator("gems_battle")
gems_battle = {}
gems_battle.team_names = { -- not translatable
    "ruby", -- red
    "emerald", --green
    "sapphire", --blue
    "opal", --yellow
}
gems_battle.team_names_caps = { -- translatable
    S("Ruby"), -- red
    S("Emerald"), --green
    S("Sapphire"), --blue
    S("Opal"), --yellow
}

arena_lib.register_minigame("gems_battle", {
    name = S("Gems Battle"),
    icon = "item_ruby.png",
    prefix = "[Gems]",
    show_minimap = true,
    teams = gems_battle.team_names,
    variable_teams_amount = true,
    teams_color_overlay = { "orange", "green", "blue", "yellow"},
    properties = {
        ruby_shop = vector.zero(),
        emerald_shop = vector.zero(),
        sapphire_shop = vector.zero(),
        opal_shop = vector.zero(),
        ruby_gem = vector.zero(),
        sapphire_gem = vector.zero(),
        emerald_gem = vector.zero(),
        opal_gem = vector.zero(),
        jail = vector.zero(),
    },
    team_properties = {
        gem_exists = true,
        gem_hp = 32,
        team_eliminated = false,
        under_attack = 0,
        attack_flash = false,
    },
    temp_properties = {
        is_started = false,
    },
    player_properties = {
        in_jail = false,
        jail_time = 0,
        last_puncher = "",
    },


    regenerate_map = true,
    can_build = true,
    disabled_damage_types = {},
    damage_modifiers = {fall_damage_add_percent = -10},
    is_team_chat_default = false,
    time_mode = "decremental",
    load_time = 10,
    spectate_mode = true,
    --join_while_in_progress = true,

})

local function load(path_or_script)
    dofile(minetest.get_modpath("gems_battle") .. DIR_DELIM .. "src".. DIR_DELIM .. path_or_script .. ".lua")
end

load("api")
load("armor")
load("editor")
load("entities")
load("hud")
load("items")
load("nodes")
load("shop")
load("globalstep")
load("player_manager")
load("minigame_manager")