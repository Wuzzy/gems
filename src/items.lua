local S = minetest.get_translator("gems_battle")
local gem_names = gems_battle.team_names
local gem_names_caps = gems_battle.team_names_caps

local gem_tools = {}
local gem_reveals = {}


for _, gem_name in pairs(gem_names) do

    local color = gems_battle.get_color_code(gem_name)

    -- craftitems
    minetest.register_craftitem("gems_battle:".. gem_name , {
        description = minetest.colorize(color, gem_names_caps[_]),
        inventory_image = "item_"..gem_name..".png",
    })

        --====================  Revealer  =========================
    -- use to reveal the current locations of all team members of that gem team

    local reveal_tool_itemname = "gems_battle:".. gem_name .. "_reveal"
    table.insert(gem_tools, reveal_tool_itemname)
    table.insert(gem_reveals, reveal_tool_itemname)
    minetest.register_tool(reveal_tool_itemname, {
        description = minetest.colorize( color,S("@1 Revealer", gem_names_caps[_])) .. S("@\nUse to reveal the current location@\nof all @1 team members to your team.@\nDisappears after 5 sec and does not update.",minetest.colorize( color, gem_names_caps[_])),
        inventory_image = "gems_reveal.png^[multiply:"..color.."",
        on_use = function(itemstack, user, pointed_thing)
            if not user then return end
            if not user:is_player() then return end
            local p_name = user:get_player_name()
            if not arena_lib.is_player_playing(p_name, "gems_battle") then return end

            local wear_to_add = 65529
            -- don't use if using would break it
            if itemstack:get_wear() + wear_to_add >= 65530 then return end


            local arena = arena_lib.get_arena_by_player(p_name)

            --returns the id of the team whose color the item is
            local t_team_id = gems_battle.get_team_id_by_name(arena , gem_name)

            if not t_team_id then 
                minetest.chat_send_player(p_name,S("@1 Team isn't in this game!", gem_names_caps[_]))
                itemstack:take_item()
                -- refund the money
                user:get_inventory():add_item(ItemStack(reveal_tool_itemname .. " " .. gems_battle.prices.gems_stuff[gem_name][reveal_tool_itemname]))
                return itemstack
            end

            itemstack:add_wear(wear_to_add)

            --returns the team_id of the user
            if not arena.players[p_name] then return end
            if not arena.players[p_name].teamID then return end
            local p_team_id = arena.players[p_name].teamID
            
            gems_battle.show_hud_team_location(arena, p_team_id , t_team_id)


            return itemstack
        end,
        groups = {not_in_creative_inventory = 1}
           
        })

    --====================  Picks  =========================
    -- picks are used to break blocks of the other gems, but they are easiest to use when turned against their own color. They are
    -- the only way to break gem blocks and Great gems, and the fastest way to break gem glass. 

    --groupcaps
    local gc = {cracky = {times={[1]=2.4, [2]=1.2, [3]=0.60}, uses=20, maxlevel=3},}
    
    gc[ gem_name ] = {times={[1]=25, [2]=13, [3]=4}, uses=20, maxlevel=3}

    local pick_dg = {fleshy=1,gem=2,gem_pick=1}
    pick_dg[gem_name] = 3
    

    local gem_pick_itemname = "gems_battle:pick_" .. gem_name
    table.insert(gem_tools,gem_pick_itemname)

    minetest.register_tool(gem_pick_itemname, {
        description = minetest.colorize(color , S("@1 Pickaxe",gem_names_caps[_])) .. S("@\nUse to break Gem blocks and Great Gems, especially @1 Great Gems.",gem_name),
        inventory_image = "pick_"..gem_name..".png",
        tool_capabilities = {
            full_punch_interval = 0.9,
            max_drop_level=3,
            groupcaps = gc ,
            damage_groups = pick_dg,
        },
        sound = {breaks = "default_tool_breaks"},
        groups = {pickaxe = 1 , 
            not_in_creative_inventory = 1,
        },
    })

    

        --====================  Swords  =========================
    -- gem swords are the best swords available, and they are most effective against players of the same gem team.
    -- 

    local sw_dg = {fleshy=7,gem=1}
    sw_dg[gem_name] = 2

    local gem_sword_itemname = "gems_battle:sword_" .. gem_name
    table.insert(gem_tools,gem_sword_itemname)

    minetest.register_tool(gem_sword_itemname, {
        description = minetest.colorize(color , S("@1 Sword",gem_names_caps[_])) .. S("@\nMost useful against the @1 team",gem_name),
        inventory_image = "sword_"..gem_name..".png",
        tool_capabilities = {
            full_punch_interval = 0.7,
            max_drop_level=1,
            groupcaps={
                snappy={times={[1]=2.0, [2]=1.00, [3]=0.35}, uses=30, maxlevel=3},
            },
            damage_groups = sw_dg,
        },
        sound = {breaks = "default_tool_breaks"},
        groups = {sword = 1, 
            not_in_creative_inventory = 1,
        },
    })
end

gems_battle.gem_reveals = gem_reveals --globalize so time tick can read it

-- make gem attacks more effective
minetest.register_on_player_hpchange(function(player, hp_change, reason)
    local p_name = player:get_player_name()
    if not p_name then return hp_change end
    if (not( arena_lib.is_player_in_arena(p_name ,"gems_battle")) or arena_lib.is_player_spectating(p_name)) then
        return hp_change
    end

    local arena = arena_lib.get_arena_by_player(p_name)

    if not arena.players[ p_name ] then return hp_change end

    local p_team_id = arena.players[ p_name ].teamID
    local p_team_name = arena.teams[p_team_id].name

    if reason and reason.type == "punch" and reason.object and reason.object:is_player() then
        local puncher = reason.object
        local puncher_name = puncher:get_player_name()
        if ( not (arena_lib.is_player_in_arena( puncher_name , "gems_battle" ))) then
            return hp_change
        end

        if puncher then
            local modifier = 0
            local itemstack = puncher:get_wielded_item()
            local victim_worn_armor = armor:get_weared_armor_elements(player)
            if itemstack then 
                    if itemstack:get_name() == "gems_battle:sword_ruby" then
                        --give 1 more damage
                        if p_team_name == "ruby" then
                            modifier = modifier - 1
                            gems_battle.make_special_damage_particles(vector.add(player:get_pos(),vector.new(0,1,0)), "ruby")
                        end
                    end
                    if itemstack:get_name() == "gems_battle:sword_emerald" then
                        if p_team_name == "emerald" then
                            modifier = modifier - 1
                            gems_battle.make_special_damage_particles(vector.add(player:get_pos(),vector.new(0,1,0)), "emerald")
                        end
                    end
                    if itemstack:get_name() == "gems_battle:sword_opal" then
                        if p_team_name == "opal" then
                            modifier = modifier - 1
                            gems_battle.make_special_damage_particles(vector.add(player:get_pos(),vector.new(0,1,0)), "opal")
                        end
                    end
                    if itemstack:get_name() == "gems_battle:sword_sapphire" then
                        if p_team_name == "sapphire" then
                            modifier = modifier - 1
                            gems_battle.make_special_damage_particles(vector.add(player:get_pos(),vector.new(0,1,0)), "sapphire")
                        end
                    end

                hp_change = hp_change + modifier

            end
        end
    end

    return hp_change
    
end, true)





--TODO: gem arrows, add to hp_change

--TODO: gem armor
