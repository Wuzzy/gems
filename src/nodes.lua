local S = minetest.get_translator("gems_battle")
local gem_names = gems_battle.team_names
local gem_names_caps = gems_battle.team_names_caps


for _ , gem_name in pairs(gem_names) do

    local gem_name_cap = gem_names_caps[_]


        
    --==================   BLOCKS   ====================== 

    --blocks made from gems cannot be broken except with picks made from
    --gems_battle. Its easiest to break them with the pick of the same gem.

        
    minetest.register_node( "gems_battle:block_".. gem_name, {
        description =  S("@1 Block",gem_name_cap),
        tiles = {"block_"..gem_name..".png"},
        groups = gems_battle.get_dig_groups(gem_name),
        sounds = default.node_sound_glass_defaults(),
    })

    

    --==================   GLASS   ======================
    --Glass made from gems cant be broken with regular tools, but it only slows gem tools down a bit
    
    minetest.register_node( "gems_battle:glass_".. gem_name, {
        description = S("@1 Glass",gem_name_cap),
        tiles = {"glass_"..gem_name..".png"},
        groups = gems_battle.get_dig_groups(gem_name,true), -- soft=true
        paramtype = "light",
        use_texture_alpha = "blend",
        sunlight_propagates = true,
        sounds = default.node_sound_glass_defaults(),
        drawtype = "glasslike",
    })

    
    --==================   Mines   ======================

    minetest.register_node( "gems_battle:mine_".. gem_name, {
        description = S("@1 Mine",gem_name_cap),
        tiles = { "default_stone.png^item_" .. gem_name .. ".png" },
        sounds = default.node_sound_stone_defaults(),
    })


    minetest.register_abm{
        label = "gems_battle:mine_".. gem_name .." dropper",
        nodenames = {"gems_battle:mine_".. gem_name},
        interval = 3,
        chance = 1,
        action = function(pos)
            minetest.spawn_item({x=pos.x, y=pos.y+1, z=pos.z}, "gems_battle:".. gem_name)
        end,
    }



    --==================   Protectors   ======================

    -- Protectors hurt players of all other teams in a radius of 4 nodes
    local pgroups = gems_battle.get_dig_groups(gem_name)
    pgroups.gem_protector = 1
    pgroups.not_in_creative_inventory = 1

    minetest.register_node( "gems_battle:protector_" .. gem_name, {
        description = S("@1 Protector",gem_name_cap),
        tiles = {"glow_"..gem_name..".png"},
        groups = pgroups,
        paramtype = "light",
        use_texture_alpha = "blend",    
        sounds = default.node_sound_glass_defaults(),
        drawtype = "glasslike",
        after_place_node = function(pos, placer, itemstack, pointed_thing)
            if minetest.find_node_near(pos, 4, {"group:gem_protector"}, false) then
                if placer and placer:is_player() then
                    minetest.chat_send_player(placer:get_player_name(), S("You can't place protectors within 4 nodes of another protector!"))
                end
                minetest.set_node(pos,{name = "air"})
                return true
            end
            local timer = minetest.get_node_timer(pos)
            timer:start(1) -- in seconds
        end,
        on_timer = function(pos)
            local obj_in_radius = minetest.get_objects_inside_radius(pos,5)
            local players_inside_radius = {}
            if not obj_in_radius then return true end
            for _,obj in ipairs(obj_in_radius) do
                if obj and obj:is_player() then
                    local p_name = obj:get_player_name()
                    if arena_lib.is_player_playing(p_name, "gems_battle") then
                        table.insert(players_inside_radius, obj)
                    end

                end
            end

            for _, player in ipairs(players_inside_radius) do
                local p_name = player:get_player_name()
                local arena = arena_lib.get_arena_by_player(p_name)
                local teamID = arena.players[p_name].teamID

                local team_name = arena.teams[teamID].name
                local weared_armor_elems = armor:get_weared_armor_elements(player)
                local item_shield = weared_armor_elems["shield"]


                if team_name ~= gem_name then
                    
                    if not(item_shield) or (item_shield ~= "gems_battle:shield_" .. gem_name) then
                        
                        local hp = player:get_hp()
                        local newhp = hp - 1
                        player:set_hp(newhp)
                    end
                end
            end
            return true

        end,
    })
end




























