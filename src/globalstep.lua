local attack_flash_timer = 0
local attack_flash = true
minetest.register_globalstep(function(d_time)
    attack_flash_timer = attack_flash_timer + d_time
    if attack_flash_timer > .5 then
        attack_flash = not(attack_flash)
        attack_flash_timer = 0
    end
    -- per active arena
    for arenaID,arena in ipairs(arena_lib.mods.gems_battle.arenas) do
        if arena.in_game then

            for t_id,t_data in ipairs(arena.teams) do
                if t_data.under_attack > 0 then
                    t_data.under_attack = t_data.under_attack - d_time
                    t_data.attack_flash = attack_flash
                else
                    t_data.attack_flash = false
                end
            end
            if arena.is_started then
                for p_name,stats in pairs(arena.players) do
                    local player = minetest.get_player_by_name(p_name)
                    if stats.in_jail then
                        stats.jail_time = stats.jail_time - d_time
                        -- if the gem is dug while the player is in jail, they are eliminated.
                        if arena.teams[stats.teamID].gem_exists == false then
                            arena_lib.remove_player_from_arena(p_name, 1, stats.last_puncher, nil)
                        elseif stats.jail_time < 0 then
                            stats.in_jail = false
                            stats.jail_time = 0
                            stats.last_puncher = nil
                            player:set_pos(arena_lib.get_random_spawner(arena, stats.teamID))
                        end
                    end
                    gems_battle.update_hud(arena,p_name)
                end
                for sp_name,stats in pairs(arena.spectators) do
                    gems_battle.update_hud(arena,sp_name,true)
                end
            end
        end
    end
end)